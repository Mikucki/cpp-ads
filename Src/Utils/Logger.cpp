#include "Logger.hpp"

#include <iomanip>
#include <iostream>
#include <string>

namespace CppAds
{
Logger* Logger::m_pLogger = NULL;

Logger::Logger()
{
}

Logger::~Logger()
{
}

Logger* Logger::GetInstance()
{
    if ( m_pLogger == NULL )
    {
        m_pLogger = new Logger();
    }

    return m_pLogger;
}

void Logger::Log( std::string text )
{
    std::cout << text << std::endl;
}

void Logger::LogBuffer( uint8_t* pBuffer, size_t length )
{
    std::cout.width( 2 );
    for ( size_t i = 0; i < length; ++i )
    {
        std::cout << std::setfill( '0' ) << std::setw( 2 ) << std::showbase
                  << std::hex << static_cast<int>( pBuffer[i] ) << " ";
    }

    std::cout << std::endl;
}
}
