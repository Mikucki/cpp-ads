#include "KL2408Output.hpp"

#include "OsAssert.hpp"

namespace CppAds
{
KL2408Output::KL2408Output( const size_t group, const size_t offset,
                            const size_t offsetSize,
                            const DataProviderIf::AccessType accessType )
    : m_Group( group ), m_Offset( offset ), m_OffsetSize( offsetSize ),
      m_AccessType( accessType )
{
}

KL2408Output::~KL2408Output()
{
}

size_t KL2408Output::GetGroup()
{
    return m_Group;
}

void KL2408Output::SetOffset( const size_t offset )
{
    m_Offset = offset;
}
size_t KL2408Output::GetOffset()
{
    return m_Offset;
}
size_t KL2408Output::GetOffsetSize()
{
    return m_OffsetSize;
}

bool KL2408Output::IsOffsetFixed()
{
    return false;
}

DataProviderIf::AccessType KL2408Output::GetAccessType()
{
    return m_AccessType;
}

bool KL2408Output::ReadByte( uint32_t group, uint32_t offset,
                             uint8_t& dataRead )
{
    // Let's assume for the moment that we get only requests for our index

    // Check that offset fits in our array.
    Osal::OsAssert( offset < MAX_LENGTH_OF_INDEX_DATA );

    // Read byte from array.
    dataRead = m_DataArray[offset];

    return true;
}

bool KL2408Output::WriteByte( uint32_t group, uint32_t offset,
                              uint8_t dataByte )
{
    // Let's assume for the moment that we get only requests for our index

    // Check that offset fits in our array.
    Osal::OsAssert( offset < MAX_LENGTH_OF_INDEX_DATA );

    // Write byte into array.
    m_DataArray[offset] = dataByte;

    // Success.
    return true;
}
}
