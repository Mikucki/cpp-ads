#include "KL1408Input.hpp"

#include "OsAssert.hpp"

namespace CppAds
{
KL1408Input::KL1408Input( const size_t group, const size_t offset,
                          const size_t offsetSize,
                          const DataProviderIf::AccessType accessType )
    : m_Group( group ), m_Offset( offset ), m_OffsetSize( offsetSize ),
      m_AccessType( accessType )
{
}

KL1408Input::~KL1408Input()
{
}

size_t KL1408Input::GetGroup()
{
    return m_Group;
}

void KL1408Input::SetOffset( const size_t offset )
{
    m_Offset = offset;
}
size_t KL1408Input::GetOffset()
{
    return m_Offset;
}
size_t KL1408Input::GetOffsetSize()
{
    return m_OffsetSize;
}

bool KL1408Input::IsOffsetFixed()
{
    return false;
}

DataProviderIf::AccessType KL1408Input::GetAccessType()
{
    return m_AccessType;
}

bool KL1408Input::ReadByte( uint32_t group, uint32_t offset, uint8_t& dataRead )
{
    // Let's assume for the moment that we get only requests for our index

    // Check that offset fits in our array.
    Osal::OsAssert( offset < MAX_LENGTH_OF_INDEX_DATA );

    // Read byte from array.
    dataRead = m_DataArray[offset];

    return true;
}

bool KL1408Input::WriteByte( uint32_t group, uint32_t offset, uint8_t dataByte )
{
    // Let's assume for the moment that we get only requests for our index

    // Check that offset fits in our array.
    Osal::OsAssert( offset < MAX_LENGTH_OF_INDEX_DATA );

    // Write byte into array.
    m_DataArray[offset] = dataByte;

    // Success.
    return true;
}
}
