#include "MemoryDataProvider.hpp"

#include "OsAssert.hpp"

namespace CppAds
{
MemoryDataProvider::MemoryDataProvider(
  const size_t group, const size_t offset, const size_t offsetSize,
  const DataProviderIf::AccessType accessType )
    : m_Group( group ), m_Offset( offset ), m_OffsetSize( offsetSize ),
      m_AccessType( accessType )
{
}

MemoryDataProvider::~MemoryDataProvider()
{
}

size_t MemoryDataProvider::GetGroup()
{
    return m_Group;
}

void MemoryDataProvider::SetOffset( const size_t offset )
{
    m_Offset = offset;
}
size_t MemoryDataProvider::GetOffset()
{
    return m_Offset;
}
size_t MemoryDataProvider::GetOffsetSize()
{
    return m_OffsetSize;
}

bool MemoryDataProvider::IsOffsetFixed()
{
    return false;
}

DataProviderIf::AccessType MemoryDataProvider::GetAccessType()
{
    return m_AccessType;
}

bool MemoryDataProvider::ReadByte( uint32_t group, uint32_t offset,
                                   uint8_t& dataRead )
{
    // Let's assume for the moment that we get only requests for our index

    // Check that offset fits in our array.
    Osal::OsAssert( offset < MAX_LENGTH_OF_INDEX_DATA );

    // Read byte from array.
    dataRead = m_DataArray[offset];

    return true;
}

bool MemoryDataProvider::WriteByte( uint32_t group, uint32_t offset,
                                    uint8_t dataByte )
{
    // Let's assume for the moment that we get only requests for our index

    // Check that offset fits in our array.
    Osal::OsAssert( offset < MAX_LENGTH_OF_INDEX_DATA );

    // Write byte into array.
    m_DataArray[offset] = dataByte;

    // Success.
    return true;
}
}
