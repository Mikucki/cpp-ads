#include "DataProviders/KL1408Input.hpp"
#include "DataProviders/KL2408Output.hpp"
#include "DataProviders/MemoryDataProvider.hpp"
#include "OsInterprocess.hpp"
#include "ProcessImage.hpp"
#include "ProcessImageMonitor.hpp"
#include "ProcessImageStimulator.hpp"
#include "Socket/SocketListener.hpp"
#include "Socket/SocketWorker.hpp"
#include "Socket/SocketWorkerParameters.hpp"

#ifndef CPPADSAPP_HPP
#define CPPADSAPP_HPP

namespace CppAds
{
class CppAdsApp
{
  public:
    CppAdsApp();
    ~CppAdsApp();

    bool Initialize();
    void Run();

  private:
    static const size_t SOCKET_WORKERS_COUNT = 1;

    SocketListener m_Listener;
    SocketWorker* m_pSocketWorkers[SOCKET_WORKERS_COUNT];

    Osal::OsQueueType m_CommandQueue;
    ProcessImage m_ProcessImage;

    KL1408Input* m_pInput0;
    KL2408Output* m_pOutput0;
    MemoryDataProvider* m_pIoMemoryR;
    MemoryDataProvider* m_pIoMemoryW;
    ProcessImageStimulator m_Stimulator;

    ProcessImageMonitor m_MonitorIn;
    ProcessImageMonitor m_MonitorOut;

    CppAdsApp( const CppAdsApp& other ) = delete;
    CppAdsApp( CppAdsApp&& other )      = delete;
    CppAdsApp& operator=( const CppAdsApp& other ) = delete;
    CppAdsApp& operator=( CppAdsApp&& other ) = delete;
};
}

#endif // CPPADSAPP_HPP
