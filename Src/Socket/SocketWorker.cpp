#include "SocketWorker.hpp"
#include "AdsCommandFactory.hpp"
#include "AmsPacket.hpp"
#include "Utils/Logger.hpp"

#include "OsAssert.hpp"
#include "OsInterprocess.hpp"
#include "OsTask.hpp"

#include "SocketWorkerParameters.hpp"

#include <iostream>

namespace CppAds
{

SocketWorker::SocketWorker()
{
}

SocketWorker::~SocketWorker()
{
}

void SocketWorker::Initialize( Osal::OsQueueType* pCommandQueue,
                               ProcessImage* pProcessImage )
{
    m_pCommandQueue = pCommandQueue;
    m_pProcessImage = pProcessImage;
}

void SocketWorker::ServiceAllCommands()
{
    Osal::Status ret = Osal::Status::OK;

    // Get command from the queue.
    SocketWorkerParameters parameters;

    while ( true )
    {
        ret = Osal::OsQueueReceiveBlocking(
          *m_pCommandQueue, reinterpret_cast<void*>( &parameters ) );

        if ( ret != Osal::Status::OK )
        {
            return;
        }
        if ( parameters.command ==
             SocketWorkerParameters::SocketWorkerCommand::FINISH )
        {
            return;
        }
        else if ( parameters.command ==
                  SocketWorkerParameters::SocketWorkerCommand::PROCESS )
        {

            ServiceSingleConnection( &parameters.socket );
        }
    }
}

void SocketWorker::ServiceSingleConnection( Osal::OsSocketType* pSocket )
{
    Osal::Status status = Osal::Status::OK;

    Osal::OsAssert( pSocket != NULL );
    Osal::OsAssert( m_pProcessImage != NULL );

    while ( status == Osal::Status::OK )
    {
        size_t dataReceivedLength             = RECEIVE_SIZE;
        uint8_t receivingBuffer[RECEIVE_SIZE] = {};
        uint8_t sendingBuffer[SEND_SIZE]      = {};

        // std::cout << "Trying to read some data." << std::endl;

        Osal::SocketStatus socketStatus = Osal::OsSocketReceive(
          *pSocket, &receivingBuffer[0], dataReceivedLength );

        if ( socketStatus == Osal::SocketStatus::OK )
        {

            // std::cout << "Read " << std::dec << dataReceivedLength << "
            // bytes." << std::endl;
            // Logger::GetInstance()->LogBuffer( &receivingBuffer[0],
            // dataReceivedLength );

            AmsPacket* pRequest = new AmsPacket();
            pRequest->BuildFromBuffer( &receivingBuffer[0],
                                       dataReceivedLength );
            // pRequest->Print();

            bool result = pRequest->Process( m_pProcessImage );
            Osal::OsAssert( result == true );

            size_t dataToSendLength = SEND_SIZE;
            // std::cout << "Trying to get response data." << std::endl;
            result =
              pRequest->GetResponseData( sendingBuffer, dataToSendLength );

            Osal::OsAssert( result == true );

            socketStatus =
              OsSocketSend( *pSocket, &sendingBuffer[0], dataToSendLength );

            Osal::OsAssert( socketStatus == Osal::SocketStatus::OK );
        }
        else
        {
            std::cout << "Read failed. Closing." << std::endl;
            Osal::OsSocketClose( *pSocket );
        }
    }
}
void SocketWorker::Run()
{
    Osal::OsTaskCreate( SocketWorker::RunThreadFunction,
                        reinterpret_cast<void*>( this ), 0xFFFF, 1,
                        "SocketWorker", m_Task );
}

void SocketWorker::RunThreadFunction( void* pParameter )
{
    SocketWorker* pInstance = reinterpret_cast<SocketWorker*>( pParameter );

    pInstance->ServiceAllCommands();
}
}
