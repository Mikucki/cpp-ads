#include "SocketListener.hpp"
#include "AdsCommandFactory.hpp"
#include "AmsPacket.hpp"
#include "SocketWorkerParameters.hpp"
#include "Utils/Logger.hpp"

#include "OsAssert.hpp"
#include <iostream>

namespace CppAds
{
SocketListener::SocketListener()
{
}

SocketListener::~SocketListener()
{
}

void SocketListener::Initialize( Osal::OsQueueType* pCommandQueue )
{
    Osal::SocketStatus status = Osal::SocketStatus::OK;
    Osal::OsAssert( pCommandQueue != NULL );

    m_pCommandQueue = pCommandQueue;

    // Create a socket object first
    status = Osal::OsSocketCreate( m_SocketListen, Osal::SocketProtocol::TCP,
                                   Osal::SocketBlocking::YES );
    if ( status != Osal::SocketStatus::OK )
    {
        std::cout << "Error when creating a socket" << std::endl;
    }
    // Open a listening server connection
    status = Osal::OsSocketListen( m_SocketListen, 0x0, 0xBF02 );
    if ( status != Osal::SocketStatus::OK )
    {
        std::cout << "Error when opening the connection" << std::endl;
    }
}

void SocketListener::Run()
{
    while ( true )
    {
        Osal::OsSocketType connectionSocket;
        Osal::SocketStatus status = Osal::SocketStatus::OK;

        std::cout << "Trying to accept a connection." << std::endl;
        status = Osal::OsSocketAccept( m_SocketListen, connectionSocket );
        if ( status != Osal::SocketStatus::OK )
        {
            std::cout << "Error when accepting the connection" << std::endl;
            break;
        }

        std::cout << "Accepted a connection" << std::endl;

        SocketWorkerParameters parameters;
        parameters.socket = connectionSocket;
        parameters.command =
          SocketWorkerParameters::SocketWorkerCommand::PROCESS;

        if ( Osal::OsQueueSendNonBlocking(
               *m_pCommandQueue, reinterpret_cast<void*>( &parameters ) ) !=
             Osal::Status::OK )
        {
            std::cout << "Couldn't send command to workers. All workers busy?"
                      << std::endl;
        }
    }
}
}
