#include "OsSocket.hpp"

#ifndef SOCKETWORKERPARAMETERS
#define SOCKETWORKERPARAMETERS

namespace CppAds
{
class ProcessImage;

struct SocketWorkerParameters
{
    enum class SocketWorkerCommand
    {
        PROCESS,
        FINISH
    } command;
    Osal::OsSocketType socket;
};
}
#endif // SOCKETWORKERPARAMETERS
