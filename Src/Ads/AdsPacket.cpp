#include "AdsPacket.hpp"
#include "AdsCommand.hpp"
#include "AdsCommandFactory.hpp"
#include "Endianness.hpp"
#include "OsAssert.hpp"
#include <iostream>

namespace CppAds
{

AdsCommandFactory AdsPacket::m_CommandFactory;

AdsPacket::AdsPacket()
{
}

AdsPacket::~AdsPacket()
{
}

bool AdsPacket::BuildFromBuffer( uint8_t* pBuffer, size_t length )
{
    Osal::OsAssert( length >= SIZE_OF_ADS_HEADER );

    uint8_t* pCurrentBufferPtr = pBuffer;
    for ( size_t i = 0; i < SIZE_OF_NETID; ++i )
    {
        m_AmsNetIdTarget[i] = *pCurrentBufferPtr;
        pCurrentBufferPtr++;
    }

    m_AmsPortTarget = HostNetworku16( ( pCurrentBufferPtr ) );
    pCurrentBufferPtr += 2;

    for ( size_t i = 0; i < SIZE_OF_NETID; ++i )
    {
        m_AmsNetIdSource[i] = *pCurrentBufferPtr;
        pCurrentBufferPtr++;
    }

    m_AmsPortSource = HostNetworku16( ( pCurrentBufferPtr ) );
    pCurrentBufferPtr += 2;

    m_CommandId = HostNetworku16( ( pCurrentBufferPtr ) );
    pCurrentBufferPtr += 2;

    m_StateFlags = HostNetworku16( ( pCurrentBufferPtr ) );
    pCurrentBufferPtr += 2;

    m_DataLength = HostNetworku32( pCurrentBufferPtr );
    pCurrentBufferPtr += 4;

    m_ErrorCode = HostNetworku32( ( pCurrentBufferPtr ) );
    pCurrentBufferPtr += 4;

    m_InvokeId = HostNetworku32( ( pCurrentBufferPtr ) );
    pCurrentBufferPtr += 4;

    m_pAdsCommand = m_CommandFactory.GetAdsCommandById( m_CommandId );
    if ( m_pAdsCommand == NULL )
    {
        return false;
    }

    Osal::OsAssert( m_DataLength == length - SIZE_OF_ADS_HEADER );
    m_pAdsCommand->BuildFromBuffer( pCurrentBufferPtr, m_DataLength );

    return true;
}

void AdsPacket::Print()
{
    std::cout << std::dec;

    std::cout << "m_AmsNetIdTarget: " << static_cast<int>( m_AmsNetIdTarget[0] )
              << "." << static_cast<int>( m_AmsNetIdTarget[1] ) << "."
              << static_cast<int>( m_AmsNetIdTarget[2] ) << "."
              << static_cast<int>( m_AmsNetIdTarget[3] ) << "."
              << static_cast<int>( m_AmsNetIdTarget[4] ) << "."
              << static_cast<int>( m_AmsNetIdTarget[5] );
    std::cout << std::endl;

    std::cout << "m_AmsPortTarget: " << m_AmsPortTarget;
    std::cout << std::endl;

    std::cout << "m_AmsNetIdSource: " << std::dec
              << static_cast<int>( m_AmsNetIdSource[0] ) << "."
              << static_cast<int>( m_AmsNetIdSource[1] ) << "."
              << static_cast<int>( m_AmsNetIdSource[2] ) << "."
              << static_cast<int>( m_AmsNetIdSource[3] ) << "."
              << static_cast<int>( m_AmsNetIdSource[4] ) << "."
              << static_cast<int>( m_AmsNetIdSource[5] );
    std::cout << std::endl;

    std::cout << "m_AmsPortSource: " << m_AmsPortSource;
    std::cout << std::endl;

    std::cout << "m_CommandId: " << std::dec << m_CommandId << std::endl;
    std::cout << "m_StateFlags: " << std::showbase << std::hex << m_StateFlags
              << std::endl;
    std::cout << "m_DataLength: " << std::dec << m_DataLength << std::endl;
    std::cout << "m_ErrorCode: " << std::dec << m_ErrorCode << std::endl;
    std::cout << "m_InvokeId: " << std::showbase << std::hex << m_InvokeId
              << std::endl;

    m_pAdsCommand->Print();
}

bool AdsPacket::Process( ProcessImage* pProcessImage )
{
    Osal::OsAssert( m_pAdsCommand != NULL );

    return m_pAdsCommand->Process( pProcessImage );
}

bool AdsPacket::GetResponseData( uint8_t* pBuffer, size_t& length )
{
    Osal::OsAssert( length >= SIZE_OF_ADS_HEADER );
    uint8_t* pCurrentBufferPtr = pBuffer;

    // Returend packet should have target/source opposite.
    for ( size_t i = 0; i < SIZE_OF_NETID; ++i )
    {
        *pCurrentBufferPtr = m_AmsNetIdSource[i];
        pCurrentBufferPtr++;
    }

    *reinterpret_cast<uint16_t*>( pCurrentBufferPtr ) =
      HostNetworku16( m_AmsPortSource );
    pCurrentBufferPtr += 2;

    for ( size_t i = 0; i < SIZE_OF_NETID; ++i )
    {
        *pCurrentBufferPtr = m_AmsNetIdTarget[i];
        pCurrentBufferPtr++;
    }

    *reinterpret_cast<uint16_t*>( pCurrentBufferPtr ) =
      HostNetworku16( m_AmsPortTarget );
    pCurrentBufferPtr += 2;

    *reinterpret_cast<uint16_t*>( pCurrentBufferPtr ) =
      HostNetworku16( m_CommandId );
    pCurrentBufferPtr += 2;

    m_StateFlags |= 0x1U;
    *reinterpret_cast<uint16_t*>( pCurrentBufferPtr ) =
      HostNetworku16( m_StateFlags );
    pCurrentBufferPtr += 2;

    // We don't know the lenght yet, let's store it later.
    uint32_t* pLengthPtr = reinterpret_cast<uint32_t*>( pCurrentBufferPtr );
    pCurrentBufferPtr += 4;

    // Continue with other elements of the packet.
    *( reinterpret_cast<uint32_t*>( pCurrentBufferPtr ) ) =
      HostNetworku32( m_ErrorCode );
    pCurrentBufferPtr += 4;

    *( reinterpret_cast<uint32_t*>( pCurrentBufferPtr ) ) =
      HostNetworku32( m_InvokeId );
    pCurrentBufferPtr += 4;

    size_t commandResponseLength = length - SIZE_OF_ADS_HEADER;
    bool result = m_pAdsCommand->GetResponseData( pCurrentBufferPtr,
                                                  commandResponseLength );

    // Set the proper length.
    *pLengthPtr = static_cast<uint32_t>( commandResponseLength );

    length = commandResponseLength + SIZE_OF_ADS_HEADER;
    return result;
}
}
