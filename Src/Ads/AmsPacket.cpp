#include "AmsPacket.hpp"
#include "OsAssert.hpp"
#include <iostream>

namespace CppAds
{

AmsPacket::AmsPacket()
{
}

AmsPacket::~AmsPacket()
{
}

bool AmsPacket::BuildFromBuffer( uint8_t* pBuffer, size_t length )
{
    Osal::OsAssert( length >= SIZE_OF_AMS_HEADER );
    m_Reserved = HostNetworku16( pBuffer );
    if ( m_Reserved != 0x0000U )
    {
        return false;
    }

    uint32_t len =
      *reinterpret_cast<uint32_t*>( ( pBuffer + sizeof( m_Reserved ) ) );
    m_Length = HostNetworku32( len );

    Osal::OsAssert( length >= SIZE_OF_AMS_HEADER + m_Length );
    m_AdsPacket.BuildFromBuffer(
      pBuffer + sizeof( m_Reserved ) + sizeof( m_Length ), m_Length );

    return true;
}

void AmsPacket::Print()
{
    std::cout << "Reserved: " << std::showbase << std::hex << m_Reserved
              << std::endl;
    std::cout << "Length: " << std::dec << m_Length << std::endl;

    m_AdsPacket.Print();
}

bool AmsPacket::Process( ProcessImage* pProcessImage )
{
    return m_AdsPacket.Process( pProcessImage );
}

bool AmsPacket::GetResponseData( uint8_t* pBuffer, size_t& length )
{
    Osal::OsAssert( length >= SIZE_OF_AMS_HEADER );
    uint8_t* pCurrentBufferPtr = pBuffer;

    // Reserved should always be zero.
    *reinterpret_cast<uint16_t*>( pBuffer ) = HostNetworku16( m_Reserved );
    pCurrentBufferPtr += 2;

    // We don't know length yet, let's wait for that information.
    uint32_t* pLengthPtr = reinterpret_cast<uint32_t*>( pCurrentBufferPtr );
    pCurrentBufferPtr += 4;

    // Get the rest of the data from other layers.
    size_t adsResponseLength = length - SIZE_OF_AMS_HEADER;
    bool result =
      m_AdsPacket.GetResponseData( pCurrentBufferPtr, adsResponseLength );

    // Now that we know size, store it.
    *pLengthPtr = adsResponseLength;
    length      = adsResponseLength + SIZE_OF_AMS_HEADER;
    return result;
}
}
