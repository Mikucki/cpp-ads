#include "AdsCommand.hpp"
#include "AdsCommandReadWrite.hpp"
#include "AdsCommandWriteControl.hpp"
#include <cstring>

#ifndef ADSCOMMANDFACTORY_HPP
#define ADSCOMMANDFACTORY_HPP

namespace CppAds
{
class AdsCommandFactory
{
  public:
    AdsCommandFactory();
    ~AdsCommandFactory();

    AdsCommand* GetAdsCommandById( uint16_t commandId );

  private:
    static const size_t MAX_COMMANDS_REGISTERED            = 10;
    AdsCommand* m_CommandRegistry[MAX_COMMANDS_REGISTERED] = {};
    AdsCommandReadWrite m_CommandReadWrite;
    AdsCommandWriteControl m_CommandWriteControl;
};
};

#endif // ADSCOMMANDFACTORY_HPP
