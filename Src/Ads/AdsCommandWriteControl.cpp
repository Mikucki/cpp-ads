#include "AdsCommandWriteControl.hpp"
#include "Endianness.hpp"
#include "OsAssert.hpp"
#include <algorithm>
#include <cstring>
#include <iostream>

namespace CppAds
{
AdsCommandWriteControl::AdsCommandWriteControl()
{
}

AdsCommandWriteControl::~AdsCommandWriteControl()
{
}

AdsCommand::CommandId AdsCommandWriteControl::GetId()
{
    return m_CommandId;
}

bool AdsCommandWriteControl::BuildFromBuffer( uint8_t* pBuffer, size_t length )
{

    m_IsPingOnly = ( length == 0 );

    if ( m_IsPingOnly )
    {
        return true;
    }

    Osal::OsAssert( length >= SIZE_OF_COMMAND_HEADER_WRITECONTROL );

    uint8_t* pCurrentBufferPtr = pBuffer;

    m_AdsState = HostNetworku16( ( pCurrentBufferPtr ) );
    pCurrentBufferPtr += 2;

    m_DeviceState = HostNetworku16( pCurrentBufferPtr );
    pCurrentBufferPtr += 2;

    m_DataLength = HostNetworku32( pCurrentBufferPtr );
    pCurrentBufferPtr += 4;

    // Make sure that lenght of read data is correct and that
    // we can fit it in our buffer.
    Osal::OsAssert( length - SIZE_OF_COMMAND_HEADER_WRITECONTROL ==
                    m_DataLength );
    Osal::OsAssert( m_DataLength <= COMMAND_REQUEST_DATA_SIZE );

    // Copy command data from the packet.
    std::memcpy( m_RequestData, pCurrentBufferPtr,
                 std::min( m_DataLength, static_cast<uint32_t>(
                                           COMMAND_REQUEST_DATA_SIZE ) ) );
    m_RequestDataLength = std::min(
      m_DataLength, static_cast<uint32_t>( COMMAND_REQUEST_DATA_SIZE ) );

    return true;
}

bool AdsCommandWriteControl::Process( ProcessImage* pProcessImage )
{
    // Currently we don't use command data in Write Control,
    // so just set the result in respnse.

    *reinterpret_cast<uint32_t*>( &m_ResponseData[0] ) =
      static_cast<uint32_t>( AdsCommand::AdsError::NO_ERROR );
    m_ResponseDataLength = 4U;

    m_IsProcessed = true;
    return true;
}

void AdsCommandWriteControl::Print()
{
    std::cout << std::endl;
    std::cout << "Command: Write Control" << std::endl;
    if ( m_IsPingOnly )
    {
        std::cout << "Only a ping" << std::endl;
    }
    else
    {
        std::cout << "Full command packet" << std::endl;
        std::cout << "Ads state: " << std::hex << m_AdsState << std::endl;
        std::cout << "Device state: " << std::hex << m_DeviceState << std::endl;
        std::cout << "Data length: " << std::dec << m_DataLength << std::endl;
    }
}
}
