#include "AdsCommand.hpp"
#include "AdsCommandFactory.hpp"
#include <cstdint>
#include <cstring>

#include "ProcessImage.hpp"

#ifndef ADSPACKET_HPP
#define ADSPACKET_HPP

namespace CppAds
{
class AdsPacket
{
  public:
    AdsPacket();
    ~AdsPacket();

    bool BuildFromBuffer( uint8_t* pBuffer, size_t length );
    void Print();
    bool GetResponseData( uint8_t* pBuffer, size_t& length );
    bool Process( ProcessImage* pProcessImage );

  private:
    /// Data fields in the packet

    // AMSNetId Target
    uint8_t m_AmsNetIdTarget[6] = {};

    // AMSPort Target
    uint16_t m_AmsPortTarget = 0x0000;

    // AMSNetId Source
    uint8_t m_AmsNetIdSource[6] = {};

    // AMSPort Source
    uint16_t m_AmsPortSource = 0x0000;

    // CommandId
    uint16_t m_CommandId = 0x0000;

    // State flags
    uint16_t m_StateFlags = 0x0000;

    // Length
    uint32_t m_DataLength = 0;

    // Error code
    uint32_t m_ErrorCode = 0;

    // Invoke Id
    uint32_t m_InvokeId = 0;

    // Data
    static const size_t MAX_DATA_LENGTH = 1024;

    AdsCommand* m_pAdsCommand = NULL;

    static const size_t SIZE_OF_ADS_HEADER = 32;
    static const size_t SIZE_OF_NETID      = 6;

    static AdsCommandFactory m_CommandFactory;
};
};

#endif // ADSPACKET_HPP
