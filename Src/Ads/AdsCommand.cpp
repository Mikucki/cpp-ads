#include "AdsCommand.hpp"
#include <algorithm>
#include <cstring>

namespace CppAds
{

AdsCommand::AdsCommand()
{
}

AdsCommand::~AdsCommand()
{
}

bool AdsCommand::GetResponseData( uint8_t* pBuffer, size_t& length )
{
    size_t size = AdsCommand::COMMAND_REQUEST_DATA_SIZE;
    if ( m_IsProcessed )
    {
        std::memcpy( pBuffer, m_ResponseData,
                     std::min( m_ResponseDataLength, size ) );
        length = std::min( m_ResponseDataLength, size );
        return true;
    }
    else
    {
        length = 0;
        return false;
    }
}
}
